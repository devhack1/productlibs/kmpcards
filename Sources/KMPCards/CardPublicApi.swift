//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import UIKit

public struct CardFactory {
    public init() {}
    public func makeSome(card: CardModelProtocol) -> UIViewController {
        let vc = CardVC()
        vc.apiClient = .init(cardModel: card)
        return vc
    }
}

public struct CardDataProviderFactory {
    public init() {}
    public func makeProvider() -> CardsDataProviderProtocol {
        return CardsDataProvider()
    }
}

public protocol CardModelProtocol {
    var image: UIImage? { get }
    var name: String { get }
    var amount: String { get }
    var pincode: String { get }
}

public protocol CardsDataProviderProtocol {
    func loadData(completion: @escaping ([CardModelProtocol]) -> Void)
}
