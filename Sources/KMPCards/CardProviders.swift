//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import DSKit

struct CardApiClient {
    var cardModel: CardModelProtocol
    func loadData(completion: @escaping (CardModelProtocol) -> Void) {
        completion(cardModel)
    }
}

struct CardsDataProvider: CardsDataProviderProtocol {
    func loadData(completion: @escaping ([CardModelProtocol]) -> Void) {
        completion([
            CardModel(image: .card, name: "4212 81** **** 2122", amount: " 90 112, 04 Р", pincode: " Установить ПИН-код"),
            CardModel(image: .card, name: "4215 98** **** 8812", amount: " 322 112, 04 Р", pincode: " Обновить ПИН-код"),
        ])
    }
}
