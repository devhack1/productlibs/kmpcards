//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 06.06.2021.
//

import UIKit

public struct CardModel: CardModelProtocol {
    public let image: UIImage?
    public let name: String
    public let amount: String
    public let pincode: String

    public init(
        image: UIImage? = nil,
        name: String? = "",
        amount: String? = "",
        pincode: String? = ""
    ) {
        self.image = image
        self.name = name ?? ""
        self.amount = amount ?? ""
        self.pincode = pincode ?? ""
    }
}
