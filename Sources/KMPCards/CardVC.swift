//
//  File.swift
//  
//
//  Created by Вильян Яумбаев on 05.06.2021.
//

import UIKit
import DSKit
import SnapKit
import KMPCore

class CardVC: UIViewController {

    var apiClient: CardApiClient?

    let stack = UIStackView()
    let imageView = UIImageView()
    let amountLabel = UILabel()
    let pincodeLabel = UILabel()
    let creditBanner = CreditBannerView()

    override func loadView() {
        stack.axis = .vertical
        stack.alignment = .center
        stack.spacing = 16
        stack.addArrangedSubview(creditBanner)
        stack.addArrangedSubview(imageView)
        imageView.snp.makeConstraints {
            $0.width.equalToSuperview().multipliedBy(0.4)
            $0.height.equalTo(imageView.snp.width).multipliedBy(0.5)
        }
        [
            amountLabel,
            pincodeLabel,
        ].forEach {
            $0.numberOfLines = 0
            stack.addArrangedSubview($0)
        }

        let view = UIScrollView()
        view.attributeBackgroundColor = ColorAttribute.backgroundPrimary
        view.addSubview(stack)
        stack.snp.makeConstraints {
            $0.leading.trailing.equalToSuperview().inset(CGFloat.leftInset)
            $0.top.equalTo(view.contentLayoutGuide.snp.topMargin).inset(24)
            $0.bottom.lessThanOrEqualTo(view.contentLayoutGuide.snp.bottomMargin).inset(24)
        }
        view.contentLayoutGuide.snp.makeConstraints {
            $0.width.equalTo(view.snp.width)
        }
        self.view = view
    }

    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ThemeManager.shared.setApplicationTheme(traitCollection.userInterfaceStyle)
    }

    public override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        ThemeManager.shared.setApplicationTheme(traitCollection.userInterfaceStyle)
        view.backgroundColor = ThemeManager.shared.currentTheme.getColor(.backgroundPrimary)
        loadData()
    }

    @objc func openCredits() {
        UIApplication.shared.open(URL(string: "kmpdeep://credits")!, options: [:], completionHandler: nil)
    }

    func loadData() {
        apiClient?.loadData { (card) in
            self.setupData(card: card)
        }
        creditBanner.label.text = "Самые актуальные и выгодные предложения для держателей банковских карт, сниженные ставки по жилищным и потребительским кредитам, другие привлекательные предложения - все то, что нельзя пропустить!"
        creditBanner.button.setTitle("Хочу кредит!", for: .normal)
        creditBanner.button.addTarget(self, action: #selector(openCredits), for: .touchUpInside)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Card"
        loadData()
    }

    func setupData(card: CardModelProtocol) {
        title = card.name
        imageView.image = card.image
        imageView.contentMode = .scaleAspectFit
        let color = ThemeManager.shared.currentTheme.getColor(.textPrimary) ?? .black
        amountLabel.attributedText = card
            .amount
            .attributed
            .alignment(.left)
            .font(Style.Font.blackCustom(value: 22).font)
            .color(color)
        pincodeLabel.attributedText = card
            .pincode
            .attributed
            .alignment(.left)
            .font(Style.Font.blackCustom(value: 16).font)
            .color(color)
    }
}
