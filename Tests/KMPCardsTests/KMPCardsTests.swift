import XCTest
@testable import KMPCards

final class KMPCardsTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(KMPCards().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
