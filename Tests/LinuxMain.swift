import XCTest

import KMPCardsTests

var tests = [XCTestCaseEntry]()
tests += KMPCardsTests.allTests()
XCTMain(tests)
